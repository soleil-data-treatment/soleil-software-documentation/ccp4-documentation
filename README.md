# Aide et ressources de CCP4 pour Synchrotron SOLEIL

[<img src="http://www.ccp4.ac.uk/download/logos/ccp4-small-logo-transparent.png" width="150"/>](https://www.ccp4.ac.uk)

## Résumé

- traiter données cristallographiques, raffiner/résoudre/mélanger/manipuler des structures cristallographiques
- Open source

## Sources

- Code source:  http://www.ccp4.ac.uk/download/#os=src
- Documentation officielle:  https://www.ccp4.ac.uk/?page_id=200

## Navigation rapide

| Tutoriaux | Page pan-data |
| - | - |
| [Tutoriel d'installation officiel](http://www.ccp4.ac.uk/download/#os=windows) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/125/ccp4) |
| [Tutoriaux officiels](https://www.ccp4.ac.uk/?page_id=1072) |   |

## Installation

- Systèmes d'exploitation supportés: Windows,  MacOS
- Installation: Difficile (très technique),  pas mal de dépendances 

## Format de données

- en entrée: mtz,  text hkl,  cif,  structures .pdb,  cartes
- en sortie: mtz,  pdb,  text,  html
- sur un disque dur,  sur la Ruche,  sur la Ruche locale
